//
//  CategoriesCellViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct CategoryCellViewModel {
    
    var categoryName: String
    var categoryColor: String
    
    init(withCategory category: Category) {
        categoryName = category.name
        categoryColor = category.color.colorName
    }
    
}
