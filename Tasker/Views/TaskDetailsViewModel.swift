//
//  TaskDetailsViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct TaskDetailsViewModel {
    
    var task: Task!
    
    var formattedDate: String {
        if let task = self.task, let date = task.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd.MM.yyyy - HH:mm"
            return dateFormatter.string(from: date)
        }
        return ""
    }
    
}
