//
//  TaskViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class TaskViewModel {

    private var tasks = [Task]()
    
    lazy var fetchedResultsController: NSFetchedResultsController<TaskEntity> = {
        let fetchRequest: NSFetchRequest<TaskEntity> = TaskEntity.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "title", ascending: true)
        let descriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = descriptors
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CustomContext.shared.getContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return frc
    }()

}

extension TaskViewModel {
    
    func numberOfSections() -> Int {
        return fetchedResultsController.sections!.count
    }
    
    
    func numberOfTasks(inSection section: Int) -> Int {
        return fetchedResultsController.sections![section].numberOfObjects
    }
    
    
    func getTaskCell(indexPath: IndexPath) -> TaskCellViewModel {
        let taskEntity = fetchedResultsController.object(at: indexPath)
        return TaskCellViewModel(withTask: taskEntity.convertToModel())
    }
    
    
    func getTask(indexPath: IndexPath) -> Task {
        let taskEntity = fetchedResultsController.object(at: indexPath)
        return taskEntity.convertToModel()
    }
    
    
    func setTaskAsDone(indexPath: IndexPath, completion: @escaping (_ success: Bool, _ error: String?) -> Void) {
        let taskEntity = fetchedResultsController.object(at: indexPath)
        taskEntity.isDone = true
        do {
            try CustomContext.shared.getContext.save()
            completion(true, nil)
        } catch {
            completion(false, error.localizedDescription)
        }
    }
    
    
    func getAllTasks() -> [Task] {
        if let taskEntities = fetchedResultsController.fetchedObjects {
            return taskEntities.map { $0.convertToModel() }
        }
        return []
    }
    
    
    func delete(atIndexPath indexPath: IndexPath) {
        let taskEntity = fetchedResultsController.object(at: indexPath)
        CustomContext.shared.getContext.delete(taskEntity)
        CustomContext.shared.saveContext()
    }
    
    
    func initDefaultCategories(completion: @escaping (AppResult<String>) -> Void) {
        UserDefaults.standard.set(true, forKey: Constants.notFirstLauch)
        let moc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        moc.persistentStoreCoordinator = CustomContext.shared.getContext.persistentStoreCoordinator
        moc.perform {
            Category.getCategories().forEach { category in
                let categoryEntity = CategoryEntity(context: moc)
                categoryEntity.name = category.name
                categoryEntity.color = category.color.colorName
            }
            do {
                try moc.save()
                completion(.success(""))
            } catch {
                completion(.error(error.localizedDescription))
            }
        }
    }
  
}
