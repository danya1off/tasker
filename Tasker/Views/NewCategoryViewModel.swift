//
//  NewCategoryViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/16/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class NewCategoryViewModel {
    
    private var _category = Category()
    var categoryEntity: CategoryEntity!
    
}

extension NewCategoryViewModel {
    
    var category: Category {
        get {
            return _category
        }
        set {
            _category = newValue
        }
    }
    
    
    var categoryName: String {
        get {
            return _category.name
        }
        set {
            _category.name = newValue
        }
    }
    
    
    var categoryColorName: String {
        get {
            return _category.color.colorName
        }
        set {
            _category.color.colorName = newValue
        }
    }
    
    
    func save(completion: @escaping (AppResult<Any>) -> Void) {
        var context = CustomContext.shared.getContext
        do {
            if categoryEntity == nil {
                categoryEntity = CategoryEntity(context: context)
            } else {
                context = categoryEntity.managedObjectContext!
            }
            if !categoryName.isEmpty {
                categoryEntity.name = categoryName
            }
            if !categoryColorName.isEmpty {
                categoryEntity.color = categoryColorName
            }
            
            try context.save()
            completion(.success(""))
        } catch {
            completion(.error(error.localizedDescription))
        }
    }
    
    
    func validation() -> AppResult<String> {
        if categoryName.isEmpty {
            return .error("Category name can't be empty!")
        }
        if categoryColorName.isEmpty {
            return .error("Color can't be empty!")
        }
        return .success("")
    }
    
}
