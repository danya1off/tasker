//
//  NewTaskViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/13/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import CoreData

class NewTaskViewModel {
    
    private var _categories = [Category]()
    private var _task = Task()
    
}

extension NewTaskViewModel {
    
    var task: Task {
        get {
            return _task
        }
        set {
            _task = newValue
        }
    }
    
    
    var id: String {
        get {
            return _task.id
        }
        set {
            _task.id = newValue
        }
    }
    
    
    var title: String {
        get {
            return _task.title
        }
        set {
            _task.title = newValue
        }
    }
    
    
    var date: Date? {
        get {
            return _task.date
        }
        set {
            _task.date = newValue
        }
    }
    
    
    var content: String {
        get {
            return _task.content
        }
        set {
            _task.content = newValue
        }
    }
    
    
    var isDone: Bool {
        get {
            return _task.isDone
        }
        set {
            _task.isDone = newValue
        }
    }
    
    
    var categoryName: String {
        get {
            return _task.category.name
        }
        set {
            _task.category.name = newValue
        }
    }
    
    
    var color: String {
        get {
            return _task.category.color.colorName
        }
        set {
            _task.category.color.colorName = newValue
            _task.category.color.color = Constants.colors[newValue]!.color
        }
    }
    
    
    func getCategories(completion: @escaping (AppResult<[Category]>) -> Void) {
        let fetchedRequest: NSFetchRequest<CategoryEntity> = CategoryEntity.fetchRequest()
        do {
            let categorieEntities = try CustomContext.shared.getContext.fetch(fetchedRequest)
            _categories = CategoryEntity.convertToModel(data: categorieEntities)
            completion(.success(_categories))
        } catch {
            completion(.error(error.localizedDescription))
        }
    }
    
    
    func save(completion: @escaping (AppResult<Task>) -> Void) {
        let context = CustomContext.shared.getContext
        let taskEntity: TaskEntity!
        do {
            if _task.id != "" {
                let taskFetchRequest: NSFetchRequest<TaskEntity> = TaskEntity.fetchRequest()
                taskFetchRequest.predicate = NSPredicate(format: "id == %@", _task.id)
                taskEntity = try context.fetch(taskFetchRequest).first!
            } else {
                taskEntity = TaskEntity(context: context)
                let id = UUID().uuidString
                taskEntity.id = id
                task.id = id
            }
            
            taskEntity.title = task.title
            taskEntity.date = task.date as NSDate?
            taskEntity.content = task.content
            if let date = task.date, date > Date() {
                taskEntity.isDone = false
            } else {
                taskEntity.isDone = task.isDone
            }
            let categoryFetchRequest: NSFetchRequest<CategoryEntity> = CategoryEntity.fetchRequest()
            categoryFetchRequest.predicate = NSPredicate(format: "name == %@", _task.category.name)
            let categories = try context.fetch(categoryFetchRequest)
            if (!categories.isEmpty) {
                let category = categories.first!
                category.addToTask(taskEntity)
                taskEntity.category = category
            }

            try context.save()
            completion(.success(task))
        } catch {
            completion(.error(error.localizedDescription))
        }
    }

    
    func validation() -> AppResult<Any> {
        if task.title.isEmpty {
            return .error("Title can't be empty!")
        }
        if task.date == nil {
            return .error("Date can't be empty!")
        }
        if task.category.name.isEmpty {
            return .error("Category name can't be empty!")
        }
        return .success("")
    }
    
    
    func numberOfCategories() -> Int {
        return _categories.count
    }
    
    
    var categories: [Category] {
        return _categories
    }
    
    
    var colors: [String] {
        return Array(Constants.colors.keys)
    }
    
    
    func numberOfColors() -> Int {
        return colors.count
    }
    
    
    func getColorName(index: Int) -> String {
        return categories[index].color.colorName
    }
    
    
    func getCategory(index: Int) -> Category {
        return _categories[index]
    }

}
