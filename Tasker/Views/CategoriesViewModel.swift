//
//  CategoriesViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import  CoreData

class CategoriesViewModel {
    
    lazy var fetchedResultsController: NSFetchedResultsController<CategoryEntity> = {
        let fetchRequest: NSFetchRequest<CategoryEntity> = CategoryEntity.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        let descriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = descriptors
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CustomContext.shared.getContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return frc
    }()
    
}


extension CategoriesViewModel {
    
    func numberOfSections() -> Int {
        return fetchedResultsController.sections!.count
    }
    
    func numberOfCategories(inSection section: Int) -> Int {
        return fetchedResultsController.sections![section].numberOfObjects
    }
    
    func getCategoryCell(indexPath: IndexPath) -> CategoryCellViewModel {
        let categoryEntity = fetchedResultsController.object(at: indexPath)
        return CategoryCellViewModel(withCategory: categoryEntity.convertToModel())
    }
    
    func getCategory(indexPath: IndexPath) -> CategoryEntity {
        return fetchedResultsController.object(at: indexPath)
    }
    
}
