//
//  TaskCellViewModel.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct TaskCellViewModel {
    
    var title: String
    var categoryName: String
    var categoryColor: String
    var date: Date
    var isDone: Bool
    
    init(withTask task: Task) {
        title = task.title
        categoryName = task.category.name
        categoryColor = task.category.color.colorName
        date = task.date ?? Date()
        isDone = task.isDone
    }
    
}
