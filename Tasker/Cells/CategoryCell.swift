//
//  CategoryCell.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryColorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        categoryColorView.layer.cornerRadius = 5
    }
    
    
    var viewModel: CategoryCellViewModel! {
        didSet {
            categoryName.text = viewModel.categoryName
            if let color = Constants.colors[viewModel.categoryColor] {
                categoryColorView.backgroundColor = color.color
            }
        }
    }

}
