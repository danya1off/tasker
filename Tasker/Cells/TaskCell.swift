//
//  TaskCell.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var progress: UILabel!
    @IBOutlet weak var categoryColorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    var viewModel: TaskCellViewModel! {
        didSet {
            title.text = viewModel.title
            category.text = viewModel.categoryName
            date.text = convert()
            progress.text = viewModel.isDone ? "Done" : "In Progress"
            progress.textColor = viewModel.isDone ? UIColor.green : UIColor.red
            if let color = Constants.colors[viewModel.categoryColor] {
                categoryColorView.backgroundColor = color.color
            }
        }
    }
    
    
    private func convert(date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        return dateFormatter.string(from:date)
    }
    
    
    private func convert() -> String {
        let currentDate = convert(date: Date())
        let taskDate = convert(date: viewModel.date)
        
        if currentDate == taskDate {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            return "Today, \(dateFormatter.string(from: viewModel.date))"
        }
        return taskDate
    }
    
}
