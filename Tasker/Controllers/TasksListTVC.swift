//
//  ViewController.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

class TasksListTVC: UIViewController  {

    @IBOutlet weak var tableView: UITableView!
    
    private(set) var viewModel: TaskViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = TaskViewModel()
        customSettings()
        getTasksSorted(by: "title")
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    
    // get all task on app launch or when sorting by segmented control
    private func getTasksSorted(by descriptor: String) {
        do {
            viewModel.fetchedResultsController.delegate = self
            let descriptor = NSSortDescriptor(key: descriptor, ascending: true)
            viewModel.fetchedResultsController.fetchRequest.sortDescriptors = [descriptor]
            try viewModel.fetchedResultsController.performFetch()
        } catch {
            self.errorAlert(withMessage: error.localizedDescription)
        }
    }
    
    
    // sorting with segmented control
    @IBAction func sorting(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            getTasksSorted(by: "title")
            tableView.reloadData()
        } else {
            getTasksSorted(by: "date")
            tableView.reloadData()
        }
    }
    
    
    // segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == Constants.newTaskSegue {
                let navigationController = segue.destination as! UINavigationController
                let viewController = navigationController.topViewController as! NewTaskVC
                viewController.viewModel = NewTaskViewModel()
                viewController.updateNotificationsDelegate = self
            }
            if identifier == Constants.showTaskSegue {
                let viewController = segue.destination as! TaskDetailsVC
                viewController.viewModel = TaskDetailsViewModel(task: sender as! Task)
            }
            if identifier == Constants.settingsSegue {
                let navigationController = segue.destination as! UINavigationController
                let viewController = navigationController.topViewController as! SettingsTVC
                viewController.viewModel = SettingsViewModel()
                viewController.viewModel.taskViewModel = self.viewModel
            }
        }
    }
}


// MARK: - NSFetchedResultsControllerDelegate
extension TasksListTVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .automatic)
            }
            break
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            break
        default:
            break
        }
    }
    
}


// MARK: - TableView methods
extension TasksListTVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numberOfRows = viewModel.numberOfTasks(inSection: section)
        if numberOfRows == 0 {
            tableView.backgroundView?.isHidden = false
            return numberOfRows
        } else {
            tableView.backgroundView?.isHidden = true
            return numberOfRows
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.taskCell, for: indexPath) as! TaskCell
        cell.viewModel = viewModel.getTaskCell(indexPath: indexPath)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            let task = self.viewModel.getTask(indexPath: indexPath)
            self.removeUserNotification(identifier: task.id)
            self.viewModel.delete(atIndexPath: indexPath)
        }
        let done = UITableViewRowAction(style: .default, title: "Done") { (action, indexPath) in
            self.viewModel.setTaskAsDone(indexPath: indexPath, completion: { (success, error) in
                if !success {
                    self.errorAlert(withMessage: error!)
                } else {
                    let task = self.viewModel.getTask(indexPath: indexPath)
                    self.removeUserNotification(identifier: task.id)
                }
            })
        }
        
        delete.backgroundColor = .red
        done.backgroundColor = .orange
        return [delete, done]
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.showTaskSegue, sender: viewModel.getTask(indexPath: indexPath))
    }
    
}

// MARK: - UNUserNotificationCenterDelegate
extension TasksListTVC: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert])
    }
    
    
    fileprivate func removeUserNotification(identifier: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
    }
    
    
    fileprivate func configureUserNotifications(completion: @escaping (_ success: Bool) -> Void) {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) { allowed, error in}
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        let tasks = viewModel.getAllTasks()
        if !tasks.isEmpty && UserDefaults.standard.bool(forKey: Constants.isNotificationsEnabled) {
            tasks.forEach({ task in
                self.addTaskToNotificationCenter(task: task) {completion($0)}
            })
        }
    }
    
    
    fileprivate func addTaskToNotificationCenter(task: Task, completion: @escaping (_ success: Bool) -> Void) {
        let notification = UNMutableNotificationContent()
        notification.title = task.title
        if !task.content.isEmpty {
            notification.body = task.content
        }
        
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: task.date!)
        
        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: task.id, content: notification, trigger: notificationTrigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            if error != nil {
                completion(false)
            } else {
                completion(true)
            }
        })
    }
    
}

// MARK: - Custom Settings
extension TasksListTVC {
    
    fileprivate func customSettings() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundView = TableViewBackgroundView()
        tableView.separatorStyle = .none
        navigationController?.navigationBar.shadowImage = UIImage.init()
        
        configureUserNotifications { result in
            if !result {
                self.errorAlert(withMessage: "Can't configure notifications!")
            }
        }
        UNUserNotificationCenter.current().delegate = self
        
        let notFirstLauch = UserDefaults.standard.bool(forKey: Constants.notFirstLauch)
        if !notFirstLauch {
            initializeDefaultCategories()
        }
    }
    
    private func initializeDefaultCategories() {
        viewModel.initDefaultCategories { [unowned self] result in
            switch result {
            case .error(let errorMsg):
                DispatchQueue.main.async { [unowned self] in
                    self.errorAlert(withMessage: errorMsg)
                }
            default:
                break
            }
        }
    }
    
}

// MARK: - UpdateNotificationsProtocol
extension TasksListTVC: UpdateNotificationsDelegate {
    
    func addNotification(forTask task: Task) {
        if UserDefaults.standard.bool(forKey: Constants.isNotificationsEnabled) {
            DispatchQueue.global(qos: .userInteractive).async {
                self.addTaskToNotificationCenter(task: task) { success in
                    if !success {
                        self.errorAlert(withMessage: "Can't configure notifications!")
                    }
                }
            }
        }
    }
    
}
