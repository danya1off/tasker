//
//  CategoriesTVC.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import CoreData

class CategoriesTVC: UITableViewController {

    private(set) var viewModel: CategoriesViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = CategoriesViewModel()
        customUI()
        getCategories()
    }
    
    
    private func getCategories() {
        do {
            viewModel.fetchedResultsController.delegate = self
            try viewModel.fetchedResultsController.performFetch()
        } catch {
            errorAlert(withMessage: "Can't fetch categories")
        }
    }

    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCategories(inSection: section)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.categoryCell, for: indexPath) as! CategoryCell
        cell.viewModel = viewModel.getCategoryCell(indexPath: indexPath)
        return cell
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Constants.addNewCategorySegue, sender: viewModel.getCategory(indexPath: indexPath))
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == Constants.addNewCategorySegue {
                let viewController = segue.destination as! NewCategoryVC
                viewController.viewModel = NewCategoryViewModel()
                if let categoryEntity = sender as? CategoryEntity {
                    viewController.viewModel.categoryEntity = categoryEntity
                }
            }
        }
    }

}

extension CategoriesTVC {
    
    func customUI() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "New", style: .plain, target: self, action: #selector(addNewCategory))
    }
    
    
    @objc private func addNewCategory() {
        performSegue(withIdentifier: Constants.addNewCategorySegue, sender: nil)
    }
    
}


// MARK: - NSFetchedResultsControllerDelegate
extension CategoriesTVC: NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
            break
        case .update:
            if let indexPath = indexPath {
                tableView.reloadRows(at: [indexPath], with: .fade)
            }
            break
        default:
            break
        }
        
    }
    
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }
    
}
