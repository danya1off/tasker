//
//  TaskDetailsVC.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import UserNotifications

class TaskDetailsVC: UIViewController, UpdateTaskDetailsViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryColorView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var notifOnOffSwitch: UISwitch!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var warningLabel: UILabel!
    
    var viewModel: TaskDetailsViewModel!
    private var navTitle = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        populateData()
        
        navigationItem.title = navTitle
        contentTextView.layer.cornerRadius = 5

    }
    
    
    func populateData() {
        categoryColorView.layer.cornerRadius = 5
        editBtn.layer.cornerRadius = 5
        
        if let task = viewModel.task {
            titleLabel.text = task.title
            categoryNameLabel.text = task.category.name
            dateLabel.text = viewModel.formattedDate
            contentTextView.text = task.content
            if let color = Constants.colors[task.category.color.colorName] {
                categoryColorView.backgroundColor = color.color
            }
            navTitle = "Task Details" + (task.isDone ? " (Done)" : "")
            
            if UserDefaults.standard.bool(forKey: Constants.isNotificationsEnabled) {
                if UserDefaults.standard.bool(forKey: task.id) {
                    notifOnOffSwitch.isEnabled = true
                    warningLabel.isHidden = true
                } else {
                    notifOnOffSwitch.isOn = false
                    warningLabel.isHidden = true
                }
            } else {
                notifOnOffSwitch.isEnabled = false
                warningLabel.isHidden = false
            }
            
            
            
        } else {
            errorAlert(withMessage: "Task data is empty!")
        }
    }
    
    
    @IBAction func switchNotifications(_ sender: UISwitch) {
        UserDefaults.standard.set(sender.isOn, forKey: viewModel.task.id)
        if sender.isOn {
            updateNotification(forTask: viewModel.task, completion: { success in
                if !success {
                    self.errorAlert(withMessage: "Can't configure notifications!")
                }
            })
        } else {
            removeNotification(withIdentifier: viewModel.task.id)
        }
    }
    
    
    @IBAction func editTask(_ sender: UIButton) {
        if let task = viewModel.task {
            performSegue(withIdentifier: Constants.editTaskSegue, sender: task)
        } else {
            errorAlert(withMessage: "Task data is empty!")
        }
    }
    
    
    // update details view after task updating
    func updateDetailsView(task: Task) {
        self.viewModel.task = task
        populateData()
        if UserDefaults.standard.bool(forKey: task.id) {
            notifOnOffSwitch.isOn = true
            updateNotification(forTask: task) { success in
                if !success {
                    self.errorAlert(withMessage: "Can't configure notifications!")
                }
            }
        } else {
            notifOnOffSwitch.isOn = false
            removeNotification(withIdentifier: task.id)
        }
    }
    

    private func updateNotification(forTask task: Task, completion: @escaping (_ success: Bool) -> Void) {
        removeNotification(withIdentifier: task.id)
        if UserDefaults.standard.bool(forKey: Constants.isNotificationsEnabled) {
            let notification = UNMutableNotificationContent()
            notification.title = task.title
            if !task.content.isEmpty {
                notification.body = task.content
            }
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: task.date!)
            let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
            let request = UNNotificationRequest(identifier: task.id, content: notification, trigger: notificationTrigger)
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
                if error != nil {
                    completion(false)
                } else {
                    completion(true)
                }
            })
        }
        completion(true)
    }
    
    
    private func removeNotification(withIdentifier identifier: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identifier])
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == Constants.editTaskSegue {
                let navigationController = segue.destination as! UINavigationController
                let viewController = navigationController.topViewController as! NewTaskVC
                viewController.viewModel = NewTaskViewModel()
                viewController.viewModel.task =  sender as! Task
                viewController.updateTaskDetailsDelegate = self
            }
        }
    }

}
