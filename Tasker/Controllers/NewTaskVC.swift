//
//  NewTaskVC.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

protocol UpdateNotificationsDelegate: class {
    func addNotification(forTask task: Task)
}

protocol UpdateTaskDetailsViewDelegate: class {
    func updateDetailsView(task: Task)
}

class NewTaskVC: UIViewController {
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var categoryColorView: UIView!
    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var saveUpdateBtn: UIBarButtonItem!
    @IBOutlet weak var notificationSwitcher: UISwitch!
    @IBOutlet weak var warningLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private let datePicker = UIDatePicker()
    private let categoryPicker = UIPickerView()
    weak var updateNotificationsDelegate: UpdateNotificationsDelegate?
    weak var updateTaskDetailsDelegate: UpdateTaskDetailsViewDelegate?
    
    var viewModel: NewTaskViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customUI()
        getCategories()
        populateData()
    }
    
    
    // if ID is not null we show all data in related text fields and controls
    private func populateData() {
        if viewModel.id != "" {
            saveUpdateBtn.title = "Update"
            titleTextField.text = viewModel.title
            noteTextView.text = viewModel.content
            dateTextField.text = format(date: viewModel.date!)
            categoryTextField.text = viewModel.categoryName
            if let color = Constants.colors[viewModel.color] {
                categoryColorView.backgroundColor = color.color
            }
            notificationSwitcher.isOn = UserDefaults.standard.bool(forKey: viewModel.id)
        }
    }
    
    
    // get all categories for set them into PickerView
    private func getCategories() {
        viewModel.getCategories { result in
            switch result {
            case .success(_):
                break
            case .error(let errorMsg):
                self.errorAlert(withMessage: errorMsg)
            }
        }
    }

    
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    // turn on/off notification
    @IBAction func switchNotifications(_ sender: UISwitch) {
        if sender.isOn {
            updateNotification(forTask: viewModel.task, completion: { success in
                if !success {
                    self.errorAlert(withMessage: "Can't configure notifications!")
                } else {
                    UserDefaults.standard.set(true, forKey: self.viewModel.task.id)
                }
            })
        } else {
            UserDefaults.standard.set(false, forKey: viewModel.task.id)
            removeNotification(withIdentifier: viewModel.task.id)
        }
    }
    
    
    // save new task or update existing one
    @IBAction func save(_ sender: UIBarButtonItem) {
        switch viewModel.validation() {
        case .success(_):
            viewModel.save { [unowned self] result in
                switch result {
                case .success(let task):
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(self.notificationSwitcher.isOn, forKey: task.id)
                        if let delegate = self.updateNotificationsDelegate {
                            delegate.addNotification(forTask: task)
                        }
                        if let delegate = self.updateTaskDetailsDelegate {
                            delegate.updateDetailsView(task: self.viewModel.task)
                        }
                        self.dismiss(animated: true, completion: nil)
                    }
                case .error(let error):
                    DispatchQueue.main.async { [unowned self] in
                        self.errorAlert(withMessage: error)
                    }
                }
            }
        case .error(let errorMsg):
            DispatchQueue.main.async { [unowned self] in
                self.errorAlert(withMessage: errorMsg)
            }
        }
    }
    
    
    // update notification for task
    private func updateNotification(forTask task: Task, completion: @escaping (_ success: Bool) -> Void) {
        removeNotification(withIdentifier: task.id)
        let notification = UNMutableNotificationContent()
        notification.title = task.title
        if !task.content.isEmpty {
            notification.body = task.content
        }
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: task.date!)
        let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
        let request = UNNotificationRequest(identifier: task.id, content: notification, trigger: notificationTrigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
            if error != nil {
                completion(false)
            } else {
                completion(true)
            }
        })
    }
    
    
    // remove all notifications
    private func removeNotification(withIdentifier identifier: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [identifier])
    }
    
}


// MARK: - Custom Settings
extension NewTaskVC {
    
    func customUI() {
        titleTextField.delegate = self
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
        noteTextView.delegate = self
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: categoryTextField.frame.height))
        categoryTextField.delegate = self
        categoryTextField.leftView = leftView
        categoryTextField.leftViewMode = .always
        categoryTextField.inputView = UIView.init()
        
        noteTextView.layer.cornerRadius = 5
        noteTextView.layer.borderColor = UIColor.lightGray.cgColor
        noteTextView.layer.borderWidth = 0.3
        
        categoryColorView.backgroundColor = UIColor.white
        categoryColorView.layer.cornerRadius = 4
        categoryColorView.layer.borderWidth = 0.5
        categoryColorView.layer.borderColor = UIColor.lightGray.cgColor
        
        dateTextField.delegate = self
        datePicker.datePickerMode = .dateAndTime
        datePicker.locale = Locale(identifier: "en_GB")
        
        let isNotificationsEnabled = UserDefaults.standard.bool(forKey: Constants.isNotificationsEnabled)
        notificationSwitcher.isEnabled = isNotificationsEnabled
        warningLabel.isHidden = isNotificationsEnabled
        
        configureDatePicker()
        configureCategoryPicker()
        hideKeyboard()
        
    }
    
    
    private func configureDatePicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(datePickerDoneBtn))
        toolbar.setItems([doneBtn], animated: false)
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
    }
    
    
    @objc private func datePickerDoneBtn() {
        dateTextField.text = format(date: datePicker.date)
        viewModel.date = datePicker.date
        view.endEditing(true)
    }
    
    
    private func configureCategoryPicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(categoryPickerDoneBtn))
        toolbar.setItems([doneBtn], animated: false)
        categoryTextField.inputAccessoryView = toolbar
        categoryTextField.inputView = categoryPicker
    }
    
    
    @objc private func categoryPickerDoneBtn() {
        let category = viewModel.getCategory(index: categoryPicker.selectedRow(inComponent: 0))
        categoryTextField.text = category.name
        categoryColorView.backgroundColor = category.color.color
        viewModel.categoryName = category.name
        viewModel.color = category.color.colorName
        view.endEditing(true)
    }
    
    
    private func format(date d: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return dateFormatter.string(from: d)
    }
    
    func hideKeyboard() {
        let keyboardDismissTap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(keyboardDismissTap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}


// MARK: - UITextFieldDelegate
extension NewTaskVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if textField == titleTextField {
            viewModel.title = newString
        }
        if textField == dateTextField || textField == categoryTextField {
            return false
        }
        return true
    }
    
}


// MARK: - UITextViewDelegate
extension NewTaskVC: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if textView == self.noteTextView {
            
            viewModel.content = newText
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 100) , animated: true)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0) , animated: true)
    }
    
}


// MARK: - PickerView
extension NewTaskVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.numberOfCategories()
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let category = viewModel.getCategory(index: row)
        categoryColorView.backgroundColor = category.color.color
        categoryTextField.text = category.name
        viewModel.categoryName = category.name
        viewModel.color = category.color.colorName
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let mainView = UIView()
        let categoryName: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = self.viewModel.categories[row].name
            return label
        }()
        mainView.addSubview(categoryName)
        let colorView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = self.viewModel.categories[row].color.color
            view.layer.cornerRadius = 3
            return view
        }()
        mainView.addSubview(colorView)
        colorView.heightAnchor.constraint(equalTo: categoryName.heightAnchor).isActive = true
        colorView.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 0.1).isActive = true
        colorView.centerYAnchor.constraint(equalTo: categoryName.centerYAnchor).isActive = true
        colorView.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: pickerView.frame.width/4).isActive = true
        categoryName.centerYAnchor.constraint(equalTo: mainView.centerYAnchor).isActive = true
        categoryName.leftAnchor.constraint(equalTo: colorView.rightAnchor, constant: 10).isActive = true
        return mainView
    }
    
}
