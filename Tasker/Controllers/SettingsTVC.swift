//
//  SettingsTVC.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit
import UserNotifications

class SettingsTVC: UITableViewController {
    
    @IBOutlet weak var notificationSwitcher: UISwitch!
    var viewModel: SettingsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        notificationSwitcher.isOn = UserDefaults.standard.bool(forKey: Constants.isNotificationsEnabled)
        notificationsModeChange(notificationSwitcher)
        
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return 1
        }
    }
    
    
    @IBAction func close(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    
    @IBAction func notificationsModeChange(_ sender: UISwitch) {
        if sender.isOn {
            configureUserNotifications(completion: { success in
                if !success {
                    self.errorAlert(withMessage: "Can't configure notifications!")
                } else {
                    UserDefaults.standard.set(true, forKey: Constants.isNotificationsEnabled)
                }
            })
        } else {
            UserDefaults.standard.set(false, forKey: Constants.isNotificationsEnabled)
            removeNotification()
        }
    }
    
    
    private func removeNotification() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    
    private func configureUserNotifications(completion: @escaping (_ success: Bool) -> Void) {
        let tasks = viewModel.taskViewModel.getAllTasks()
        if !tasks.isEmpty {
            tasks.forEach({ task in
                let notification = UNMutableNotificationContent()
                notification.title = task.title
                if !task.content.isEmpty {
                    notification.body = task.content
                }
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: task.date!)
                let notificationTrigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: false)
                let request = UNNotificationRequest(identifier: task.id, content: notification, trigger: notificationTrigger)
                UNUserNotificationCenter.current().add(request, withCompletionHandler: { error in
                    if error != nil {
                        completion(false)
                    } else {
                        completion(true)
                    }
                })
            })
        }
        completion(true)
    }

}
