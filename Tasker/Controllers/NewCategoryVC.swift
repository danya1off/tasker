//
//  NewCategoryVC.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/15/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class NewCategoryVC: UIViewController {

    @IBOutlet weak var categoryNameTextField: UITextField!
    @IBOutlet weak var categoryColorTextField: UITextField!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var categoryColorView: UIView!
    
    var colorPicker = UIPickerView()
    let colors = Array(Constants.colors.keys)
    var viewModel: NewCategoryViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customUI()
        populateData()
    }
    
    
    private func populateData() {
        if let categoryEntity = viewModel.categoryEntity {
            if let categoryName = categoryEntity.name {
                categoryNameTextField.text = categoryName
                viewModel.categoryName = categoryName
            }
            if let colorName = categoryEntity.color, let color = Constants.colors[colorName] {
                categoryColorTextField.text = color.colorName
                categoryColorView.backgroundColor = color.color
                viewModel.categoryColorName = color.colorName
            }
        }
    }
    
    
    @IBAction func cancel(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func add(_ sender: UIButton) {
        switch viewModel.validation() {
        case .success(_):
            viewModel.save { [unowned self] result in
                switch result {
                case .success(_):
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: nil)
                    }
                case .error(let error):
                    self.errorAlert(withMessage: error)
                }
            }
        case .error(let errorMsg):
            errorAlert(withMessage: errorMsg)
        }
    }

}


// MARK: - PickerView
extension NewCategoryVC: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func customUI() {
        categoryNameTextField.delegate = self
        
        let leftView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: categoryColorTextField.frame.height))
        categoryColorTextField.delegate = self
        categoryColorTextField.leftView = leftView
        categoryColorTextField.leftViewMode = .always
        
        categoryColorView.backgroundColor = UIColor.white
        categoryColorView.layer.cornerRadius = 4
        categoryColorView.layer.borderWidth = 0.5
        categoryColorView.layer.borderColor = UIColor.lightGray.cgColor
        
        cancelBtn.layer.cornerRadius = 5
        addBtn.layer.cornerRadius = 5
        backgroundView.layer.cornerRadius = 5
        
        colorPicker.delegate = self
        colorPicker.dataSource = self
        
        configureColorPicker()
    }
    
    
    private func configureColorPicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(colorPickerDoneBtn))
        toolbar.setItems([doneBtn], animated: false)
        
        categoryColorTextField.inputView = colorPicker
        categoryColorTextField.inputAccessoryView = toolbar
    }
    
    
    @objc private func colorPickerDoneBtn() {
        if let color = Constants.colors[colors[colorPicker.selectedRow(inComponent: 0)]] {
            categoryColorTextField.text = color.colorName
            categoryColorView.backgroundColor = color.color
            viewModel.categoryColorName = color.colorName
        }
        view.endEditing(true)
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if let color = Constants.colors[colors[row]] {
            categoryColorTextField.text = color.colorName
            categoryColorView.backgroundColor = color.color
            viewModel.categoryColorName = color.colorName
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let mainView = UIView()
        let colorName: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            if let color = Constants.colors[colors[row]] {
                label.text = color.colorName
            }
            return label
        }()
        mainView.addSubview(colorName)
        
        let colorView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            if let color = Constants.colors[colors[row]] {
                view.backgroundColor = color.color
            }
            view.layer.cornerRadius = 3
            return view
        }()
        mainView.addSubview(colorView)
        
        colorView.heightAnchor.constraint(equalTo: colorName.heightAnchor).isActive = true
        colorView.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 0.1).isActive = true
        colorView.centerYAnchor.constraint(equalTo: colorName.centerYAnchor).isActive = true
        colorView.leftAnchor.constraint(equalTo: mainView.leftAnchor, constant: pickerView.frame.width/4).isActive = true
        colorName.centerYAnchor.constraint(equalTo: mainView.centerYAnchor).isActive = true
        colorName.leftAnchor.constraint(equalTo: colorView.rightAnchor, constant: 10).isActive = true
        
        return mainView
    }
    
}


// MARK: - UITextFieldDelegate
extension NewCategoryVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newText = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if textField == categoryNameTextField {
            viewModel.categoryName = newText
        }
        if textField == categoryColorTextField {
            return false
        }
        return true
    }
    
}
