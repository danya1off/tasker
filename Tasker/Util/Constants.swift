//
//  Constants.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    private init() {}
    
    // MARK: - Segue identifiers
    static let newTaskSegue = "newTaskSegue"
    static let showTaskSegue = "showTaskSegue"
    static let editTaskSegue = "editTaskSegue"
    static let settingsSegue = "settingsSegue"
    static let addNewCategorySegue = "addNewCategorySegue"
    
    // MARK: - Cell identifier
    static let taskCell = "taskCell"
    static let categoryCell = "categoryCell"
    
    // MARK: - Values
    static let notFirstLauch = "firstLauch"
    static let isNotificationsEnabled = "isNotificationsEnabled"
    
    static let colors: [String: Color] = [
    
        "Blue": Color(colorName: "Blue", color: UIColor.blue),
        "Red": Color(colorName: "Red", color: UIColor.red),
        "Brown": Color(colorName: "Brown", color: UIColor.brown),
        "Cyan": Color(colorName: "Cyan", color: UIColor.cyan),
        "Orange": Color(colorName: "Orange", color: UIColor.orange),
        "Gray": Color(colorName: "Gray", color: UIColor.gray),
        "Green": Color(colorName: "Green", color: UIColor.green),
        "Magenta": Color(colorName: "Magenta", color: UIColor.magenta),
        "Purple": Color(colorName: "Purple", color: UIColor.purple)

    ]
    
}
