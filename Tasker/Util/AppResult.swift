//
//  AppResult.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

enum AppResult<T> {
    case success(T)
    case error(String)
}
