//
//  UIViewController+Ext.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func alert(withMessage message: String) {
        let alertController = UIAlertController(title: "Success", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    
    func errorAlert(withMessage message: String) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
}
