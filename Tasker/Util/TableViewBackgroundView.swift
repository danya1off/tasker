//
//  TableViewBackgroundView.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/16/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import UIKit

class TableViewBackgroundView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    private func setupViews() {
        let imageView: UIImageView = {
            let image = UIImageView()
            image.translatesAutoresizingMaskIntoConstraints = false
            image.image = #imageLiteral(resourceName: "phone")
            image.contentMode = .scaleAspectFit
            return image
        }()
        addSubview(imageView)
        
        let text: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = "Your task list is empty :("
            label.textColor = UIColor(red:0.58, green:0.67, blue:0.72, alpha:1.0)
            label.font = UIFont(name: "HelveticaNeue-Medium", size: 17)
            return label
        }()
        addSubview(text)
        
        imageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5).isActive = true
        imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1).isActive = true
        
        text.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        text.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 20).isActive = true
    }

}
