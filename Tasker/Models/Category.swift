//
//  Category.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

struct Category {
    var name = ""
    var color = Color()
    
    // default categories
    static func getCategories() -> [Category] {
        return [
            
            Category(name: "Work", color: Color(colorName: "Red", color: UIColor.red)),
            Category(name: "Home", color: Color(colorName: "Blue", color: UIColor.blue)),
            Category(name: "Weekend", color: Color(colorName: "Cyan", color: UIColor.blue)),
            Category(name: "Holiday", color: Color(colorName: "Orange", color: UIColor.yellow))
        
        ]
    }
}
