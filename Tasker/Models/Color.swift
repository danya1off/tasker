//
//  Color.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/13/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

struct Color {
    var colorName = ""
    var color: UIColor? = UIColor.clear
}
