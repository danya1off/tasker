//
//  CategoryEntity+CoreDataProperties.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/16/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension CategoryEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CategoryEntity> {
        return NSFetchRequest<CategoryEntity>(entityName: "CategoryEntity")
    }

    @NSManaged public var color: String?
    @NSManaged public var name: String?
    @NSManaged public var task: NSSet?

}

// MARK: Generated accessors for task
extension CategoryEntity {

    @objc(addTaskObject:)
    @NSManaged public func addToTask(_ value: TaskEntity)

    @objc(removeTaskObject:)
    @NSManaged public func removeFromTask(_ value: TaskEntity)

    @objc(addTask:)
    @NSManaged public func addToTask(_ values: NSSet)

    @objc(removeTask:)
    @NSManaged public func removeFromTask(_ values: NSSet)

    static func convertToModel(data: [CategoryEntity]) -> [Category] {
        return data.map { category in
            return category.convertToModel()
        }
    }
    
    func convertToModel() -> Category {
        var category = Category()
        if let colorName = self.color, let color = Constants.colors[colorName] {
            category.color = color
        }
        if let categoryName = self.name {
            category.name = categoryName
        }
        return category
    }
    
}
