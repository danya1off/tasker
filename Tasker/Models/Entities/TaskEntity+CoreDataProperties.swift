//
//  TaskEntity+CoreDataProperties.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/16/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//
//

import Foundation
import CoreData


extension TaskEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TaskEntity> {
        return NSFetchRequest<TaskEntity>(entityName: "TaskEntity")
    }

    @NSManaged public var content: String?
    @NSManaged public var date: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var isDone: Bool
    @NSManaged public var title: String?
    @NSManaged public var category: CategoryEntity?

}

extension TaskEntity {
    
    func convertToModel() -> Task {
        var task = Task()
        if let id = self.id {
            task.id = id
        }
        if let title = self.title {
            task.title = title
        }
        if let content = self.content {
            task.content = content
        }
        task.isDone = self.isDone
        if let date = self.date {
            task.date = date as Date
        }
        if let category = self.category {
            if let categoryName = category.name {
                task.category.name = categoryName
            }
            if let colorName = category.color {
                task.category.color = Constants.colors[colorName]!
            }
        }
        return task
    }
    
}
