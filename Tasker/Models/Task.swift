//
//  Task.swift
//  Tasker
//
//  Created by Jeyhun Danyalov on 12/12/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct Task {
    
    var id = ""
    var title = ""
    var date: Date?
    var content = ""
    var isDone = false
    var category = Category()
    
}
